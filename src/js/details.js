// /* 
// 设置复用的头部黑色导航样式
// */
// $('header').load("../html/header.html");
// /* 
// 设置复用的底部样式
// */
// $('footer').load('../html/footer.html');

// /* 
// 滚动的时候设置顶部白色导航栏的样式 
//  */
// $(window).scroll(function() {
// 	if ($('html').scrollTop() >= 50) {
// 		$('nav').addClass('scrollnav');
// 	} else {
// 		$('nav').removeClass('scrollnav');
// 	}
// 	/* 
// 	 a=$(window).height：获取了窗口的高度
// 	 b=$(window).scrollTop：获取了页面顶部卷曲出去的的距离
// 	 c=$('body').height：获取了整个页面的高度
// 	 d=$('footer').offset().top：获取了footer到页面顶部的距离
// 	 关系：c-b-a<=页面底部栏footer的高度时，改变底部价格栏的className
// 	 */
// 	if ($('footer').offset().top - $(window).scrollTop() <= $(window).height()) {
// 		$('footer').removeClass('scrollfoot')
// 		$('.sum').removeClass('scrollsum')
// 	} else {
// 		$('.sum').addClass('scrollsum')
// 		$('footer').addClass('scrollfoot')
// 	}
// })
// /* 
// 回到顶部的按钮功能实现 
//  */
// $('.back').click(function() {
// 	$('html').animate({
// 		scrollTop: 0
// 	}, 600)
// })
// /* 
// 保修服务点击选中并改变样式 
//  */
// $('.options').click(function(){
// 	$(this).toggleClass('serverchecked')
// })

// 
require(['./config'], () => {
	require(['request', 'template', 'header', 'footer'], (req, template) => {
		class Details {
			constructor() {
				this.getData()
				this.scroll()
			}
			scroll() {
				$(window).scroll(function () {
					if ($('html').scrollTop() >= 50) {
						$('nav').addClass('scrollnav');
					} else {
						$('nav').removeClass('scrollnav');
					}
					/* 
					 a=$(window).height：获取了窗口的高度
					 b=$(window).scrollTop：获取了页面顶部卷曲出去的的距离
					 c=$('body').height：获取了整个页面的高度
					 d=$('footer').offset().top：获取了footer到页面顶部的距离
					 关系：c-b-a<=页面底部栏footer的高度时，改变底部价格栏的className
					 */
					if ($('footer').offset().top - $(window).scrollTop() <= $(window).height()) {
						$('footer').removeClass('scrollfoot')
						$('.sum').removeClass('scrollsum')
					} else {
						$('.sum').addClass('scrollsum')
						$('footer').addClass('scrollfoot')
					}
				})
				// 回到顶部的按钮功能实现 
				$('.back').click(function () {
					$('html').animate({
						scrollTop: 0
					}, 600)
				})
			}
			getData() {
				// 获取地址栏里面的id
				const urlArr = window.location.search.slice(1).split('&')
				const skuId = urlArr[1].split('=')[1]
				req.getDetailsList(skuId)
					.then(res => {
						// console.log(res)
						const data = res.data.list[0]
						// console.log(data)
						// 获取商品名称、价格、一些商品主图和一些其它信息
						const { name, price, sku_info } = data
						// 获取商品介绍
						const intru = data.shop_info.spu_sub_title
						// 获取商品的颜色、容量
						const colorArr = data.shop_info.spec_v2[0].spec_values
						const sizeArr = data.shop_info.spec_v2[1] && data.shop_info.spec_v2[1].spec_values || []
						// 保险服务
						const { accessory_goods } = data.product_info
						// 商品详情描述图片
						const { goods_view } = data.shop_info.tpl_content.base.images
						// 渲染导航栏
						$('nav').html(template('detailsTitle',{name}))
						// 把刚才拿到的数据整理成需要用的数据
						$('#detailsContent').html(template('detailsTemplate', { name, intru, colorArr, sizeArr, accessory_goods, sku_info, skuId }))
						// 渲染商品图
						$('.prodimg').html(template('imgTemplate', { goods_view }))
						// console.log(goods_view)
						// 渲染价格
						$('.sum').html(template('priceBarTemplate', { name, price }))

						const {ali_image,sku_id}=sku_info[0]
						// 调用加入购物车方法
						this.addToCart(name,price,ali_image,sku_id)
					})
			}
			// 加入购物车
			addToCart(name,price,img,id) {
				$('#addToCart').click(function () {
					//商品图片 价格 数量 标题 传到购物车 传到localStorage里面去
					const count=$('.inputCount').val()
					console.log(name,price,img,id,count)
					const arr=[{name,price,img,id,count}]
					// 1.先判断localStorage里面有没有这一条(list)数据
					let list=JSON.parse(localStorage.getItem('list'))
					// console.log(list)
					if(list){
						// 有这条数据
						// list是一个数组，里面有多条数据，如果有了就数量+1
						// 如果list里里面没有这条数据，就把这条数据加到list里面
						// 最后都要存储一次
						const hasData=list.some(item=>{
							return item.id===id
						})
						// console.log(hasData)
						if(hasData){
							// 代表有这条数据
							list.forEach(item=>{
								if(item.id===id){
									item.count=item.count-0+(count - 0)
									alert("成功加入购物车！")
								}
							})
						}else{
							// 没有这条数据
							list.push({name,price,img,id,count})
							alert("成功加入购物车！")
						}
						// 最后进行存储
						localStorage.setItem('list',JSON.stringify(list))
					}else{
						// 没有这条数据
						localStorage.setItem('list',JSON.stringify(arr))
						alert("成功加入购物车！")
					}
				})
			}
		}
		return new Details()
	})
})