// 专门用来写头部的加载和头部的js功能
define(['jquery'],()=>{
    // 使用了$符号说明它依赖jQuery
    class Header{
        constructor(){
            this.load();
        }
        load(){
            // 专门用来加载js文件
            $('header').load('/html/header.html')
        }
    }
    return new Header();
});