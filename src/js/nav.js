require(['request', 'template', 'jquery'], (req, template) => {
    class Nav {
        constructor() {
            this.load().then(() => {
                this.getNavData()
            })
            // this.load()后面想要使用.then()
            //只有promise对象能够使用.then
            //也就是需要this.load()内部返回一个promise对象才能使用
        }
        load() {
            return new Promise(resolve => {
                $('nav').load('/html/nav.html', resolve)
            })
        }
        // 搜索框输入数据事件
        inputChange() {
            $('.searchbar').on('input', function () {
                // 1.拿到文本框内的值
                // 2.使文本框内部的小标签隐藏
                // 3.使文本框内的x显示出来
                $('.icon-cha1').removeClass('tiphidden')
                $('.tip1').addClass('tiphidden')
                $('.tip2').addClass('tiphidden')
                $('.icon-cha1').click(function () {
                    $('.searchbar').val('')
                    $('.icon-cha1').addClass('tiphidden')
                    $('.tip1').removeClass('tiphidden')
                    $('.tip2').removeClass('tiphidden')
                })
                // 当输入框内容为空时，将tip显示，x隐藏
                if (!$(this).val()) {
                    $('.icon-cha1').addClass('tiphidden')
                    $('.tip1').removeClass('tiphidden')
                    $('.tip2').removeClass('tiphidden')
                }
                console.log($(this).val())
            })
        }
        // 渲染或调用接口获取导航栏数据
        getNavData() {
            // $.get('/cz/v1/cms/second_nav',res=>{
            //     console.log(res)
            // })
            req.nav_getData()
                .then(res => {
                    // console.log(res)
                    this.createNav(res)
                })
        }
        // 渲染导航栏数据，当数据量较大时分开渲染和获取
        createNav(res) {
            // console.log(res)
            // 提取url
            const newArr=res.map(item=>{
                const arr=item.url.split("?")[0].split("/")
                const str=arr[arr.length-1]
                item.id=str
                return item
            })
            // console.log(newArr)
            // 渲染导航栏
            $('#navContent').html(template('navTemplate', { list: newArr }))
            // 设置一个开关，用来判断鼠标是否停留再二级菜单上面
            let flag = true
            // 导航栏里面的子内容
            $('#navContent li').hover(function () {
                // 进入导航栏时打开开关
                flag=true
                // 判断当前的li是不是我渲染的li
                if ($(this).prop('class')) {
                    $('.navSub').stop().animate({ height: 266 }, 500, () => {
                        const subData = res[$(this).index()]
                        let html
                        // 短路运算符，先确定数据存在，再判断数据
                        if (subData && (subData.type === "goods")) {
                            html = template('subNavGoodsContent', { subList: subData.list })
                        } else {
                            html = template('subNavCategoryTemplate', { subList: subData.list })
                        }
                        $('.navSub').html(html)
                    })
                }
            }, () => {
                $('.navSub').hover(() => {
                    // 如果进入了二级菜单则打开开关，防止收回
                    flag = true
                },()=>{
                    // 从二级导航移出后，关闭开关，并收回导航栏
                    flag=false
                    $('.navSub').stop().animate({ height: 0 }, 500, () => {
                        $('.navSub').html('')
                    })
                })
                // 判断开关是否打开，如果关闭则将二级导航收起
                if (!flag) {
                    $('.navSub').stop().animate({ height: 0 }, 500, () => {
                        $('.navSub').html('')
                    })
                }
            })
            // 调用输入框改变事件
            this.inputChange()
        }
    }
    return new Nav()
})