// /* 
// 设置复用的头部黑色导航样式
// */
// $('header').load("./html/header.html");

// /* 
// 设置复用的灰色导航栏样式
// */
// $('nav').load('./html/nav.html');

// /* 
// 设置复用的底部样式
// */
// $('footer').load('./html/footer.html');


// /* 
// 设置顶部的轮播图样式 
//  */
// var mySwiper = new Swiper('.swiper-container', {
// 	// effect : 'fade',
// 	loop: true,
// 	autoplay: true, //可选选项，自动滑动
// 	pagination: {
// 		el: '.swiper-pagination',
// 		clickable: true,
// 	},
// });
// //如果你初始化时没有定义Swiper实例，后面也可以通过Swiper的HTML元素来获取该实例
// new Swiper('.swiper-container')
// var mySwiper = document.querySelector('.swiper-container').swiper
// mySwiper.slideNext();

// /* 
//  顶部灰色导航栏随着滚动改变样式
//  */
// $(window).scroll(function() {
// 	if ($('html').scrollTop() >= 50) {
// 		$('nav').addClass('scrollnav');
// 		$('nav li>div').addClass('scrollhide');
// 		$('nav li>b').addClass('scrollshow');
// 	} else {
// 		$('nav').removeClass('scrollnav');
// 		$('nav li>div').removeClass('scrollhide');
// 		$('nav li>b').removeClass('scrollshow');
// 	}
// })

// 先加载配置文件config.js
require(['./config'], () => {
	require(['template', 'swiper' , 'header', 'nav', 'footer', 'jquery'], (template,Swiper) => {
		var mySwiper = new Swiper('.swiper-container', {
			// effect : 'fade',
			loop: true,
			autoplay: true, //可选选项，自动滑动
			pagination: {
				el: '.swiper-pagination',
				clickable: true,
			},
		});
		// 请求数据渲染页面
		class Index {
			constructor() {
				this.getHotProdData()
				this.getForumData()
				this.scrollNav()
			}
			// 请求热门商品数据
			getHotProdData() {
				$.get('/lib/json/index_hotProducts.json', res => {
					// console.log(res)
					const html = template('hotProdTemplate', { prodlist: res.result })
					// console.log(html)
					$('#hotProductsContent').html(html)
				})
			}
			// 使用rap2模拟论坛数据
			getForumData() {
				$.get('/api/app/mock/272919/index/forum', res => {
					// console.log(res)
					if (res.code === 200) {
						$('#forumContent').html(template('forumTemplate', { dataList: res.dataList }))
					}
				})
			}
			// 页面滚动时，导航栏样式改变
			scrollNav() {
				$(window).scroll(() => {
					if ($('html').scrollTop() >= 50) {
						$('nav').addClass('scrollnav');
						$('nav li>div').addClass('scrollhide');
						$('nav li>b').addClass('scrollshow');
					} else {
						$('nav').removeClass('scrollnav');
						$('nav li>div').removeClass('scrollhide');
						$('nav li>b').removeClass('scrollshow');
					}
				})
			}
		}
		return new Index()
	})
})