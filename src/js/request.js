// 这个js用来集中管理所有的请求
//锤子：https://shopapi.smartisan.com/
define(['jquery'],()=>{
    return {
        // 发送请求
        nav_getData(){
            return new Promise(resolve=>{
                $.get('/cz/v1/cms/second_nav',(res)=>{
                    resolve(res)
                })
            })
        },
        // 获取列表页数据
        getGoodsList(id){
            return new Promise(resolve=>{
                //$https://shopapi.smartisan.com/v1/search/goods-list?category_id=896&page=1&sort=sort&num=20&type=shop&channel_id=1001
                $.get(`/cz/v1/search/goods-list?category_id=${id}&page=1&sort=sort&num=20&type=shop&channel_id=1001`,(res)=>{
                    resolve(res)
                })
            })
        },
        // 获取详情页所有数据
        getDetailsList(spuId){
            return new Promise(resolve=>{
                $.get(`/cz/product/spus?ids=${spuId}`,(res)=>{
                    resolve(res)
                })
            })
        }
    }
})