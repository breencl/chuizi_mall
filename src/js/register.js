// 判断输入是否正确的函数
/* 
判断输入框中的格式是否正确 
正则手机号码格式： pattern = /^1[34578]\d{9}$/; 
正则邮箱格式：myreg = /^([\.a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-])+/;
 */
// function isRightInput(str) {
//     const phonereg = /^1[34578]\d{9}$/
//     const emailreg = /^([\.a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-])+/
//     if (phonereg.test(str) || emailreg.test(str)) {
//         return true
//     } else {
//         return false
//     }
// }

//电话邮箱输入框失去焦点时操作
let phoneright = false//定义一个开关，当第一行邮箱和电话输入正确的时候变为true，错误时为false
let pwdright = false//定义一个开关，当密码输入了为true，否则为false
// $('#phonenum').blur(function () {
//     // console.log($('#phonenum').val())
//     if (!$(this).val()) {
//         //输入为空的时候
//         $('em').removeClass('errorshow')
//         $(this).removeClass('inputerror')
//         phoneright = false
//     } else {
//         if (!isRightInput($(this).val())) {
//             //格式错误时的操作
//             $('em').addClass('errorshow')
//             $(this).addClass('inputerror')
//             phoneright = false
//         } else {
//             phoneright = true
//         }
//     }
// })

// // 键盘输入时操作改变样式
// $('#phonenum').keyup(function () {
//     $('em').removeClass('errorshow')
//     $(this).removeClass('inputerror')
// })

// // 密码输入框键盘输入时的操作
// $('#password').keyup(function () {
//     if (!$(this).val()) {
//         //password为空的时候
//         pwdright = false
//     } else {
//         //不为空的时候
//         pwdright = true
//     }
//     if (phoneright && pwdright) {
//         $('button').addClass('rightbtn')
//     } else {
//         $('button').removeClass('rightbtn')
//     }
// })

// /* 
// 点击注册按钮时的ajax请求
// */
// $('button').click(function () {
//     //电话密码输入都正确时操作
//     if (phoneright && pwdright) {
//         $.ajax({
//             url: '../php/user/register.php',
//             type: 'post',
//             data: { phonenum: $('#phonenum').val(), password: $('#password').val() },
//             dataType: 'json',
//             success: function (res) {
//                 if (res.code === 200) {
//                     alert(res.msg)
//                     window.location.href = "../index.html"
//                 }
//                 else if (res.code === 0) {
//                     alert(res.msg)
//                     $('em').addClass('errorshow')
//                     $('#phonenum').addClass('inputerror')
//                 }
//             },
//             errror: function (error) {
//                 console.log(error)
//             }
//         })
//     }
// })

require(['./config'], () => {
    require(['jquery'], () => {
        class Register {
            constructor() {
                this.phoneright = false//定义一个开关，当第一行邮箱和电话输入正确的时候变为true，错误时为false
                this.pwdright = false//定义一个开关，当密码输入了为true，否则为false
                this.inputChange()
            }
            isRightInput(str) {
                const phonereg = /^1[34578]\d{9}$/
                const emailreg = /^([\.a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-])+/
                if (phonereg.test(str) || emailreg.test(str)) {
                    this.phoneright = true
                } else {
                    this.phoneright = false
                }
            }
            inputChange() {
                $('#phonenum').blur(() => {
                    // console.log($('#phonenum').val())
                    this.isRightInput($('#phonenum').val())
                    if (!$('#phonenum').val()) {
                        //输入为空的时候
                        $('em').removeClass('errorshow')
                        $('#phonenum').removeClass('inputerror')
                        phoneright = false
                    } else {
                        if (!this.phoneright) {
                            //格式错误时的操作
                            $('em').addClass('errorshow')
                            $('#phonenum').addClass('inputerror')
                            this.phoneright = false
                        } else {
                            this.phoneright = true
                        }
                    }
                })
                // 键盘输入时操作改变样式
                $('#phonenum').keyup(function () {
                    $('em').removeClass('errorshow')
                    $('#phonenum').removeClass('inputerror')
                })
                // 密码输入框键盘输入时的操作
                $('#password').keyup(() => {
                    if (!$('#password').val()) {
                        //password为空的时候
                        this.pwdright = false
                    } else {
                        //不为空的时候
                        this.pwdright = true
                    }
                    if (this.phoneright && this.pwdright) {
                        $('button').addClass('rightbtn')
                    } else {
                        $('button').removeClass('rightbtn')
                    }
                })
                /* 
                点击注册按钮时的ajax请求
                */
                $('button').click(() => {
                    //电话密码输入都正确时操作
                    if (this.phoneright && this.pwdright) {
                        $.ajax({
                            url: '/php/register.php',
                            type: 'post',
                            data: { phonenum: $('#phonenum').val(), password: $('#password').val() },
                            dataType: 'json',
                            success: function (res) {
                                if (res.code === 200) {
                                    alert(res.msg)
                                    window.location.href = "/"
                                }
                                else if (res.code === 0) {
                                    alert(res.msg)
                                    $('em').addClass('errorshow')
                                    $('#phonenum').addClass('inputerror')
                                }
                            },
                            errror: function (error) {
                                console.log(error)
                            }
                        })
                    }
                })
            }
        }
        return new Register()
    })
})