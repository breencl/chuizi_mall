// /* 
// 设置复用的头部黑色导航样式
// */
// $('header').load("../html/header.html");

// /* 
// 设置复用的底部样式
// */
// $('footer').load('../html/footer.html');

// /* 
// 设置结算条滚动时的样式
// 需要用到几个值：
// a=$(window).height：获取了窗口的高度
// b=$(window).scrollTop：获取了页面顶部卷曲出去的的距离
// c=$('body').height：获取了整个页面的高度
// d=$('footer').offset().top：获取了footer到页面顶部的距离
// 关系：c-b-a<=页面底部栏footer的高度时，改变底部价格栏的className
// */
// $(window).scroll(function(){
//     if($('.suggestion').offset().top-30-$(window).scrollTop() <= $(window).height()){
//         $('.allmsg').removeClass('allscroll');
//     }else{
//         $('.allmsg').addClass('allscroll');
//     }
// })

/* 
    定义成为一个模块
    去localStorage里面取数据 list
    渲染页面
*/
require(['./config'], () => {
    require(['template', 'header', 'footer', 'jquery'], (template) => {
        class Cart {
            constructor() {
                this.list = []
                this.allCount = 0
                this.allPrice = 0
                this.allSave = 0
                this.init()
                this.scrollSum()
            }
            // 初始化页面
            init() {
                // 去localStorage里面拿数据list
                this.list = JSON.parse(localStorage.getItem('list'))
                // 判断有没有数据，没有就提醒加购
                if (this.list && this.list.length) {
                    // 有数据 渲染页面
                    const html = template('cartListTemplate', { list: this.list })
                    $('.shoplist').html(html)
                    this.getCountAndAllPrice()
                    this.delProd()
                    this.changeCount()
                } else {
                    alert("购物车内暂时没有商品，请加购！")
                    location.href = "/"
                }
            }
            // 统计数量和总价
            getCountAndAllPrice() {
                this.allCount = 0
                this.allPrice = 0
                this.allSave = 0
                this.list.forEach(item => {
                    this.allCount += item.count - 0
                    this.allPrice += item.price * item.count
                    this.allSave += item.count * 9.9
                })
                // 改变DOM元素里面显示的数据
                $('.allCount').text(this.allCount)
                $('.allPrice').text(this.allPrice.toFixed(2))
                $('.allSave').text(this.allSave.toFixed(2))
            }
            // 删除商品
            // 需要把this.list数据删除一条,然后再存到localStotrage,然后重新渲染页面
            delProd() {
                const that = this
                $('.del').click(function () {
                    const index = $(this).parent().index() - 2
                    that.list.splice(index, 1)
                    localStorage.setItem('list', JSON.stringify(that.list))
                    // 重新渲染页面
                    that.init()
                })
            }
            // 编辑商品数量
            changeCount() {
                const that = this
                // 优化
                $('.computed').click(function (e) {
                    const target = e.target || e.srcElement
                    if (target.nodeName === 'BUTTON') {
                        const index = $(target).parent().parent().index() - 2
                        if ($(target).hasClass("reduce")) {
                            if (that.list[index].count === 1) {
                                that.list[index].count = 1
                                $(target).addClass(' notreduce')
                                return
                            }
                            that.list[index].count--
                        } else {
                            that.list[index].count++
                        }
                        // 从新存储数据,再渲染
                        localStorage.setItem('list', JSON.stringify(that.list))
                        that.init()
                    }
                })

                // $('.reduce').click(function () {
                //     // 找到ul的索引
                //     const index = $(this).parent().parent().index() - 2
                //     // 直接找到this.list里面对应的数据进行修改
                //     that.list[index].count--
                //     if (that.list[index].count <= 0) {
                //         that.list[index].count=1
                //         $(this).addClass('notreduce')
                //         return
                //     }
                //     // 从新存储数据,再渲染
                //     localStorage.setItem('list', JSON.stringify(that.list))
                //     that.init()
                // })
                // $('.addnum').click(function () {
                //     const index = $(this).parent().parent().index() - 2
                //     // 直接找到this.list里面对应的数据进行修改
                //     that.list[index].count++
                //     // 从新存储数据,再渲染
                //     localStorage.setItem('list', JSON.stringify(that.list))
                //     that.init()
                // })
            }
            // 设置滚动样式
            scrollSum() {
                $(window).scroll(function () {
                    if ($('.suggestion').offset().top - 30 - $(window).scrollTop() <= $(window).height()) {
                        $('.allmsg').removeClass('allscroll');
                    } else {
                        $('.allmsg').addClass('allscroll');
                    }
                })
            }
        }
        return new Cart()
    })
})