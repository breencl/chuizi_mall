// /* 
// 设置复用的头部黑色导航样式
// */
// $('header').load("../html/header.html");

// /* 
// 设置复用的头部白色导航样式
// */
// $('nav').load("../html/nav.html");

// /* 
// 设置复用的底部样式
// */
// $('footer').load('../html/footer.html');

// /* 
// 设置页面滚动时的上方白色导航栏的样式
// */
// $(window).scroll(function() {
// 	if ($('html').scrollTop() >= 50) {
// 		$('nav').addClass('scrollnav');
// 		$('nav li>div').addClass('scrollhide');
// 		$('nav li>b').addClass('scrollshow');
// 	} else {
// 		$('nav').removeClass('scrollnav');
// 		$('nav li>div').removeClass('scrollhide');
// 		$('nav li>b').removeClass('scrollshow');
// 	}
// })
require(['./config'], () => {
	require(['request', 'template', 'jquery', 'header', 'nav', 'footer'], (req, template) => {
		class List {
			constructor() {
				this.getData()
			}
			// 请求数据
			getData() {
				// 先通过地址栏的url拿到id
				const { search } = window.location
				const category_id = search.split('&')[1].split('=')[1]
				req.getGoodsList(category_id)
					.then(res => {
						// console.log(res)
						// 处理res的数据，只传要用的数据
						const { page, num, pageCount, itemCount } = res.data
						let myDataObj = { page, num, pageCount, itemCount, list: [] }
						res.data.list.forEach(item => {
							const { images, tagText, discountPrice, price, skuAttrIds, spuSubTitle, spuTitle, skuId, spuId } = item.spuInfo
							myDataObj.list.push({ images, tagText, discountPrice, price, skuAttrIds, spuSubTitle, spuTitle, skuId , spuId })
						})
						// console.log(myDataObj)
						this.createDom(myDataObj)
					})
			}
			// 渲染页面
			createDom(myDataObj) {
				const { list } = myDataObj
				$('.hotbar').html(template('listTemplate', { list }))
				// console.log(list)
				this.bindEvent(list)
				console.log(list)
			}
			// 给li绑定事件，跳转到详情页
			bindEvent(list) {
				$('.hotbar').on('click','li',function(){
					// console.log($(this))
					// 拿到当前这个li在父元素里面的索引，然后再list里面去取相应的数据
					const index=$(this).index()
					const {skuId,spuId}=list[index]
					console.log(list)
					window.location=`/html/details.html?skuId=${skuId}&spuId=${spuId}`
				})
			}
		}
		return new List()
	})
})