// 专门用来写footer加载和js操作
define(['jquery'],()=>{
    class Footer{
        constructor(){
            this.load()
        }
        load(){
            $('footer').load('/html/footer.html')
        }
    }
    return new Footer()
});