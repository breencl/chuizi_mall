const gulp = require('gulp'),
    htmlmin = require('gulp-htmlmin'),
    del = require('del')
sass = require('gulp-sass'),
    cleanCss = require('gulp-clean-css'),
    babel = require('gulp-babel'),
    uglify = require('gulp-uglify'),
    connect = require('gulp-connect'),
    { createProxyMiddleware } = require('http-proxy-middleware')

//定义一个对象，集中管理所有的路径
const path = {
    html: {
        src: 'src/**/*.html',
        dest: 'dist'
    },
    sass: {
        src: 'src/sass/**/*.scss',
        dest: 'dist/css'
    },
    jsPath: {
        src: 'src/js/**/*.js',
        dest: 'dist/js'
    },
    imgPath: {
        src: 'src/img/**/*.*',
        dest: 'dist/img'
    },
    libPath: {
        src: 'src/lib/**/**',
        dest: 'dist/lib'
    },
    iconPath: {
        src: 'src/*.ico',
        dest: 'dist'
    }
}
//定制了一个删除dist目录的任务
const delDist = () => del('dist')

const { html: html1, sass: sas, jsPath, imgPath, libPath, iconPath,phpPath } = path
// 定制html任务
const html = () => {
    return gulp.src(html1.src)
        .pipe(htmlmin({
            removeComments: true, // 清除HTML注释
            collapseWhitespace: true, // 压缩HTML
            collapseBooleanAttributes: true, // 省略布尔属性的值 <input checked="true"/> ==> <input checked />
            removeEmptyAttributes: true, // 删除所有空格作属性值 <input id="" /> ==> <input />
            removeScriptTypeAttributes: false, // 删除<script>的type="text/javascript"
            removeStyleLinkTypeAttributes: false, // 删除<style>和<link>的type="text/css"
            minifyJS: true, // 压缩页面JS
            minifyCSS: true // 压缩页面CSS 
        }))
        .pipe(gulp.dest(html1.dest))
        .pipe(connect.reload()) //监控任务执行，执行后页面自动刷新
}

// 定制css任务 1.把所有的scss转为css(node-sass、gulp-sass) 2.压缩css代码(gulp-clean-css)
const css = () => {
    return gulp.src(sas.src)
        .pipe(sass())
        .pipe(cleanCss())
        .pipe(gulp.dest(sas.dest))
        .pipe(connect.reload()) //监控任务执行，执行后页面自动刷新
}

// 定制js任务，1.把所有的ES6转为ES5(gulp-babel、@babel/core、@babel/preset-env) 2.压缩js(gulp-uglify)
const js = () => {
    return gulp.src(jsPath.src)
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(uglify())
        .pipe(gulp.dest(jsPath.dest))
        .pipe(connect.reload()) //监控任务执行，执行后页面自动刷新
}

// 把所有的img复制到dist目录
const img = () => gulp.src(imgPath.src).pipe(gulp.dest(imgPath.dest))

// 把所有的lib文件夹下面所有的东西复制到dist目录
const libs = () => gulp.src(libPath.src).pipe(gulp.dest(libPath.dest))

// 把所有的icon复制到dist目录
const icon = () => gulp.src(iconPath.src).pipe(gulp.dest(iconPath.dest))

//监控html，sass，js文件夹，当里面的文件发生改变时，再次执行任务
const watch = () => {
    gulp.watch(html1.src, html)
    gulp.watch(sas.src, css)
    gulp.watch(jsPath.src, js)
}

//开启一个服务器(gulp-connect)
const server = () => {
    connect.server({
        livereload: true,
        port: 2400,
        root: 'dist',
        middleware() {
            // 代理跨域：把以/api开头的请求代理到rap2的接口
            return [
                createProxyMiddleware('/api', {
                    target: 'http://rap2api.taobao.org/',
                    changeOrigin: true,
                    pathRewrite: {
                        '^/api': ''
                    }
                }),
                createProxyMiddleware('/cz', {
                    target: 'https://shopapi.smartisan.com/',
                    changeOrigin: true,
                    pathRewrite: {
                        '^/cz': ''
                    }
                }),
                createProxyMiddleware('/php', {
                    target: 'http://localhost/php/',
                    changeOrigin: true,
                    pathRewrite: {
                        '^/php': ''
                    }
                })
            ]
        }
    })
}

//默认任务，执行每一个任务
module.exports.default = gulp.series(delDist, gulp.parallel(html, css, js, img, libs, icon, watch, server, php))